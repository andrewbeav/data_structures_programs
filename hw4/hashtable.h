typedef struct Bucket {
  int key;
  int defunct;
  int occupied;
} Bucket;

typedef struct Hashtable {
  int N;
  int q; // Used for double hashing collision resolution method
  int size; // Number of elements currently in table
  Bucket *table;
} Hashtable;
