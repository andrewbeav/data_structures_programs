#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashtable.h"

int hash(int N, int data) {
  return data % N;
}

int double_hash(int q, int key) {
  return q - (key%q);
}

int search(Hashtable *ht, int key) {
  int index = hash(ht->N, key);
  int count = 0;

  while (((ht->table)+index)->key != key || ((ht->table)+index)->occupied != 1) {
    index += double_hash(ht->q, key);
    if (count == ht->N) return 0;
    count++;
  }
  return 1;
}

// Codes:
// -2: Duplicate key
// -1: Hashtable Full
// 0: Success
int insert(Hashtable *ht, int key) {
  if (ht->size == ht->N) return -1;
  if (search(ht, key) == 1) return -2;
  int index = hash(ht->N, key);

  while(((ht->table)+index)->occupied == 1) {
    int d = double_hash(ht->q, key);
    index = (index + d) % ht->N;
  }

  ((ht->table)+index)->key = key;
  ((ht->table)+index)->occupied = 1;
  ht->size++;

  return 0;
}

void delete(Hashtable *ht, int key) {
  int index = hash(ht->N, key);

  while(((ht->table)+index)->key != key) {
    int d = double_hash(ht->q, key);
    index += d;
  }

  ((ht->table)+index)->defunct = 1;
  ((ht->table)+index)->occupied = 0;

  ht->size--;
}

void printBucket(Bucket *bucket, int endl) {
  if (bucket->occupied == 1) printf("%d", bucket->key);
}

void printHashtable(Hashtable *ht) {
  for (int i = 0; i < ht->N; i++) {
    printf("%d: ", i);
    printBucket(((ht->table)+i), 0);
    printf("\n");
  }
  printf("\n");
}

Hashtable* initializeHashtable(int N, int q) {
  Hashtable *hashtable = (Hashtable*)malloc(sizeof(Hashtable));
  hashtable->N = N;
  hashtable->q = q;
  hashtable->table = (Bucket*)calloc(N, sizeof(Bucket));

  return hashtable;
}

int main() {
  char inputFileName[100];

  printf("Enter your input file name: ");
  scanf("%s", inputFileName);

  FILE *inputFile;
  inputFile = fopen(inputFileName, "r");

  if (inputFile == NULL) {
    printf("File not found");
    exit(1);
  }

  char command[10];

  fscanf(inputFile, "%s", command);
  int N = atoi(command);
  fscanf(inputFile, "%s", command);
  int q = atoi(command);

  Hashtable *hashtable = initializeHashtable(N, q);

  while(fscanf(inputFile, "%s", command) != EOF) {
    printf("Hashtable before operation: \n");
    printHashtable(hashtable);
    printf("\n");
    printf("Performing operation (%s)\n", command);

    char *first;
    char *second;
    first = strtok(command, ".");
    int key = atoi(first);
    second = strtok(NULL, ".");

    if (strcmp(second, "in") == 0) {
      int r = insert(hashtable, key);
      if (r == -1) {
        printf("Error: Hashtable Full. Exiting\n");
        return -1;
      }
      else if (r == -2) {
        printf("Error: Duplicate Key\n");
      }
    }
    else if (strcmp(second, "del") == 0) {
      delete(hashtable, key);
    }
    else {
      int b = search(hashtable, key);
      if (b == 1) printf("Found\n");
      else printf("Not Found\n");
    }
    printf("Hashtable after operation: \n");
    printHashtable(hashtable);

    printf("------------------------------\n\n");
  }

  fclose(inputFile);



  return 0;
}
