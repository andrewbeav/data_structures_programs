#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "heap.h"

void grow(MaxHeap *heap) {
  int *cpy = (int*)calloc(2*(heap->cur_cap), sizeof(int));
  for (int i = 0; i < heap->cur_cap; i++) {
    cpy[i] = heap->heap_arr[i];
  }
  int *tmp = heap->heap_arr;
  heap->heap_arr = cpy;
  free(tmp);
}

void upheap(MaxHeap *heap) {
  int j = (heap->size)-1;
  while (j > 0) {
    if (heap->heap_arr[(j-1)/2] > heap->heap_arr[j]) break;
    int tmp = heap->heap_arr[j];
    heap->heap_arr[j] = heap->heap_arr[(j-1)/2];
    heap->heap_arr[(j-1)/2] = tmp;
    j = (j-1)/2;
  }
}

void downheap(MaxHeap *heap) {
  int i = 0;
  while(2*i+1 <= heap->size) {
    if (heap->heap_arr[2*i+1] < heap->heap_arr[i] && heap->heap_arr[2*i+2] < heap->heap_arr[i]) {
      break;
    }

    int child_i = 0;
    if (heap->heap_arr[2*i+1] > heap->heap_arr[2*i+2]) child_i = 2*i + 1;
    else child_i = 2*i + 2;

    int tmp = heap->heap_arr[child_i];
    heap->heap_arr[child_i] = heap->heap_arr[i];
    heap->heap_arr[i] = tmp;

    i = child_i;
  }
}

void insert(MaxHeap *heap, int key) {
  if (heap->size == (heap->cur_cap)-1) grow(heap);
  heap->heap_arr[heap->size] = key;
  heap->size++;
  upheap(heap);
}

int remove_max(MaxHeap *heap) {
  int tmp = heap->heap_arr[0];
  heap->heap_arr[0] = heap->heap_arr[(heap->size)-1];
  heap->size--;
  downheap(heap);
  return tmp;
}

int search(MaxHeap *heap, int key) {
  for (int i = 0; i < heap->size; i++) {
    if (heap->heap_arr[i] == key) return 1;
  }
  return 0;
}

void prePrintr(MaxHeap *heap, int i) {
  printf("%d, ", heap->heap_arr[i]);
  if (2*i+1 < heap->size) {
    prePrintr(heap, 2*i+1);
  }
  if (2*i+2 < heap->size) {
    prePrintr(heap, 2*i+2);
  }
}

void postPrintr(MaxHeap *heap, int i) {
  if (2*i+1 < heap->size) {
    postPrintr(heap, 2*i+1);
  }
  if (2*i+2 < heap->size) {
    postPrintr(heap, 2*i+2);
  }

  printf("%d, ", heap->heap_arr[i]);
}

void inOrderPrintr(MaxHeap *heap, int i) {
  if (2*i+1 < heap->size) {
    inOrderPrintr(heap, 2*i+1);
  }
  printf("%d, ", heap->heap_arr[i]);
  if (2*i+2 < heap->size) {
    inOrderPrintr(heap, 2*i+2);
  }
}

void prePrint(MaxHeap *heap) {
  prePrintr(heap, 0);
  printf("\n");
}

void postPrint(MaxHeap *heap) {
  postPrintr(heap, 0);
  printf("\n");
}

void inOrderPrint(MaxHeap *heap) {
  inOrderPrintr(heap, 0);
  printf("\n");
}

void arrayPrint(MaxHeap *heap) {
  for (int i = 0; i < heap->size; i++) {
    printf("%d, ", heap->heap_arr[i]);
  }
  printf("\n");
}

// Codes:
// 0: PreOrder Traversal
// 1: PostOrder Traversal
// 2: In Order Traversal
// 3: Array Print (tmp)
void printHeap(MaxHeap *heap, int traversal_method) {
  if (heap->size == 0) printf("(empty heap)\n");
  else {
    switch (traversal_method) {
      case 0:
        prePrint(heap);
        break;
      case 1:
        postPrint(heap);
        break;
      case 2:
        inOrderPrint(heap);
        break;
      case 3:
        arrayPrint(heap);
      }
    }
}

MaxHeap* createHeap(int init_cap) {
  MaxHeap *heap = (MaxHeap*)malloc(sizeof(MaxHeap));
  heap->size = 0;
  heap->cur_cap = init_cap;
  heap->heap_arr = (int*)calloc(init_cap, sizeof(int));
}

int main() {
  char inputFileName[100];

  printf("Enter your input file name: ");
  scanf("%s", inputFileName);

  FILE *inputFile;
  inputFile = fopen(inputFileName, "r");

  if (inputFile == NULL) {
    printf("File not found");
    exit(1);
  }

  char command[10];

  fscanf(inputFile, "%s", command);
  int traversal_method;
  if (strcmp(command, "pre") == 0) traversal_method = 0;
  else if (strcmp(command, "post") == 0) traversal_method = 1;
  else if (strcmp(command, "arr") == 0) traversal_method = 3;
  else traversal_method = 2;

  MaxHeap *heap = createHeap(20);

  while(fscanf(inputFile, "%s", command) != EOF) {
    printf("Heap before operation: \n");
    printHeap(heap, traversal_method);
    printf("\n");
    printf("Performing operation (%s)\n", command);

    char *first;
    first = strtok(command, ".");

    if (strcmp(first, "del") == 0) {
      printf("Output: %d\n", remove_max(heap));
    }
    else {
      int key = atoi(first);
      char *second = strtok(NULL, ".");

      if (strcmp(second, "in") == 0) {
        insert(heap, key);
      }
      else if (strcmp(second, "sch") == 0) {
        switch (search(heap, key)) {
          case 0:
            printf("Not Found\n");
            break;
          default:
            printf("Found\n");
         }
      }
    }

    printf("Heap after operation: \n");
    printHeap(heap, traversal_method);

    printf("------------------------------\n\n");
  }

  fclose(inputFile);

  return 0;
}
