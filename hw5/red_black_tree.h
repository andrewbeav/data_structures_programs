/* Structs for Node and Tree */
/* -------------------------------------------*/
typedef struct Node {
  int key;
  struct Node *parent;
  struct Node *left_child;
  struct Node *right_child;
  int color; // 0: red, 1: black
  int isEmpty; // 0: False, 1: True
} Node;

typedef struct RedBlackTree {
  Node *root;
} RedBlackTree;


/* Node Creation Functions */
/* -------------------------------------------*/

Node* createNode(int k) {
  Node *node = (Node *)malloc(sizeof(Node));
  node->color = 0;
  node->key = k;
  node->isEmpty = 0;
}

Node* createLeaf() {
  Node *leaf = (Node*)malloc(sizeof(Node));
  leaf->isEmpty = 1;
  leaf->color = 1;
}

Node *createRoot(int k) {
  Node *root = createNode(k);
  root->left_child = createLeaf();
  root->right_child = createLeaf();
  root->left_child->parent = root;
  root->right_child->parent = root;
  root->color = 1;
}


/* Helper Functions */
/* -------------------------------------------*/

Node* sibling(Node* node) {
  return (node->parent->left_child == node) ? node->parent->right_child : node->parent->left_child;
}

Node* search(Node *root, int k) {
  if (root->isEmpty == 1) {
    return root;
  }
  if (k < root->key) return search(root->left_child, k);
  else if (k == root->key) return root;
  else return search(root->right_child, k);
}

int contains(RedBlackTree *tree, int k) {
  Node *result = search(tree->root, k);
  if (result->isEmpty == 1) return 0;
  return 1;
}

Node* findMinNode(Node *root) {
  if (root->left_child->isEmpty == 1) return root;
  return findMinNode(root->left_child);
}

void makeRightChild(Node *parent, Node *child) {
  parent->right_child = child;
  child->parent = parent;
}

void makeLeftChild(Node *parent, Node *child) {
  parent->left_child = child;
  child->parent = parent;
}


/* Rotations and Restructuring */
/* -------------------------------------------*/

Node* rotateRight(RedBlackTree *tree, Node *q) {
  Node *p = q->left_child;
  Node *c = q->right_child;

  if (p->parent != NULL) {
    p->parent = q->parent;
    if (q->parent->left_child = q) p->parent->left_child = p;
    else p->parent->right_child = p;
  }
  makeLeftChild(q, p->right_child);
  makeRightChild(p, q);

  if(tree->root == q) tree->root = p;

  return p;
}

Node* rotateLeft(RedBlackTree *tree, Node *p) {
  Node *a = p->left_child;
  Node *q = p->right_child;

  if (p->parent != NULL) {
    q->parent = p->parent;
    if (p->parent->left_child = p) q->parent->left_child = q;
    else q->parent->right_child = q;
  }
  makeRightChild(p, q->left_child);
  makeLeftChild(q, p);

  if(tree->root == p) tree->root = q;

  return q;
}

Node* doubleRotateRight(RedBlackTree *tree, Node *a) {
  Node *c = a->right_child;
  Node *b = c->left_child;

  makeRightChild(a, b->left_child);
  makeLeftChild(c, b->right_child);

  if (a->parent != NULL) {
    b->parent = a->parent;
    if (a->parent->left_child == a) {
      b->parent->left_child == b;
    }
    else {
      b->parent->right_child = b;
    }
  }
  makeLeftChild(b, a);
  makeRightChild(b, c);

  if(tree->root == a) tree->root = b;

  return b;
}

Node* doubleRotateLeft(RedBlackTree *tree, Node *a) {
  Node *c = a->left_child;
  Node *b = c->right_child;

  makeLeftChild(a, b->right_child);
  makeRightChild(c, b->left_child);
  if (a->parent != NULL) {
    b->parent = a->parent;
    if (a->parent->left_child == a) b->parent->left_child = b;
    else b->parent->right_child == b;
  }
  makeLeftChild(b, c);
  makeRightChild(b, a);

  if(tree->root == a) tree->root = b;

  return b;
}

Node* restructure(RedBlackTree *tree, Node *node) {
  Node *v = node->parent;
  if (v->left_child == node && v->parent->right_child == v) {
    return doubleRotateRight(tree, v->parent);
  }
  else if (v->right_child == node && v->parent->left_child == v) {
    return doubleRotateLeft(tree, v->parent);
  }
  else if (v->left_child == node && v->parent->left_child == v) {
    return rotateRight(tree, v->parent);
  }
  else {
    return rotateLeft(tree, v->parent);
  }
}


/* Insertion Functions */
/* -------------------------------------------*/

void remedy_insertion(RedBlackTree *tree, Node *node) {
  Node *v = node->parent;
  if (v->color == 0) { // Double Red
    Node *w = sibling(v);
    if (w->color == 1) { // w is black
      Node *middle = restructure(tree, node);
      tree->root->parent = NULL;
      middle->color = 1;
      middle->left_child->color = 0;
      middle->right_child->color = 0;
    }
    else { // w is red
      // Recoloring
      w->color = 1;
      v->color = 1;
      if (v->parent->parent != NULL) {
        v->parent->color = 0;
        if (v->parent->parent->color == 0) {
          remedy_insertion(tree, v->parent);
        }
      }
    }
  }
}

// Exit Codes: 0: Success, -1: Reject
int insert(RedBlackTree *tree, int k) {
  if (contains(tree, k)) return -1;
  Node *toInsert = createNode(k);
  Node *leaf = search(tree->root, k);
  if (leaf->parent->left_child == leaf) {
    makeLeftChild(leaf->parent, toInsert);
    makeLeftChild(toInsert, leaf);
    makeRightChild(toInsert, createLeaf());
  }
  else {
    makeRightChild(leaf->parent, toInsert);
    makeRightChild(toInsert, leaf);
    makeLeftChild(toInsert, createLeaf());
  }
  remedy_insertion(tree, toInsert);
  return 0;
}

/* Deletion Functions */
/* -------------------------------------------*/

void remedyDoubleBlack(RedBlackTree *tree, Node *w) {
  if (w->color == 0 && w->parent->color == 0) {
    Node *y = sibling(w);
    if (y->color == 1) { // y is black
      if (y->left_child->color == 0 || y->right_child->color == 0) {
        // Restructure, done
        Node *middle = restructure(tree, w);
        middle->color = w->parent->parent->color;
        middle->left_child->color = 1;
        middle->right_child->color = 1;
      }
      else {
        // Recoloring
        // Recursively call
        w->color = 1;
        y->color = 0;
        remedyDoubleBlack(tree, w->parent);
      }
    }
    else { // y is red
      // adjustment
      // Recursively call
      if (y->parent->right_child == y) rotateLeft(tree, y->parent);
      else rotateRight(tree, y->parent);
      y->color = 1;
      y->parent->color = 0;
      remedyDoubleBlack(tree, w);
    }
  }
}

void removeNode(RedBlackTree *tree, Node *node) {
  Node *w;
  if (node->left_child->isEmpty == 1 || node->right_child->isEmpty == 1) {
    w = (node->left_child->isEmpty == 1) ? node->left_child : node->right_child;
  }
  else {
    w = findMinNode(node);
    node->key = w->key;
    node = w;
    w = node->left_child;
  }
  Node *r = sibling(w);
  int node_color = node->color;
  int w_color = w->color;

  if (node->parent->left_child == node) node->parent->left_child = w;
  else if (node->parent->right_child == node) node->parent->right_child = w;
  w->isEmpty = 1;
  free(node->right_child);
  free(node);

  if (node_color == 0 || r->color == 0) {
    r->color == 1;
  }
  else {
    remedyDoubleBlack(tree, r);
  }
}

// Exit Codes: 0: Success, -1: Reject
int delete(RedBlackTree *tree, int k) {
  if (!contains(tree, k)) return -1;
  Node *toRemove = search(tree->root, k);
  removeNode(tree, toRemove);
  return 0;
}


/* Print Tree Functions */
void in_order_print(Node *node) {
  if (node->left_child->isEmpty == 0) {
    in_order_print(node->left_child);
  }
  printf("%d, ", node->key);
  if (node->right_child->isEmpty == 0) {
    in_order_print(node->right_child);
  }
}

void printTree(RedBlackTree *tree) {
  in_order_print(tree->root);
  printf("\n");
}
