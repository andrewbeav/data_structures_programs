#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "red_black_tree.h"

int main() {

  char inputFileName[100];

  printf("Enter your input file name: ");
  scanf("%s", inputFileName);

  FILE *inputFile;
  inputFile = fopen(inputFileName, "r");

  if (inputFile == NULL) {
    printf("File not found");
    exit(1);
  }

  char command[10];

  RedBlackTree *tree = NULL;

  while(fscanf(inputFile, "%s", command) != EOF) {
    printf("Tree before operation: \n");
    if (tree != NULL) printTree(tree);
    else printf("(empty tree)");
    printf("\n");
    printf("Performing operation (%s)\n", command);

    char *first;
    first = strtok(command, ".");

    int key = atoi(first);
    char *second = strtok(NULL, ".");

    if (strcmp(second, "in") == 0) {
      if (tree != NULL) {
        int status = insert(tree, key);
        if (status == -1) printf("Duplicate Key\n");
      }
      else {
        tree = (RedBlackTree*)malloc(sizeof(RedBlackTree));
        tree->root = createRoot(key);
      }
    }
    else if (strcmp(second, "del") == 0) {
      int status = delete(tree, key);
      if (status == -1) printf("The Key Does Not Exist\n");
    }

    printf("Tree after operation: \n");
    printTree(tree);

    printf("------------------------------\n\n");
  }

  fclose(inputFile);

  return 0;
}
