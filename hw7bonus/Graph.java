import java.util.*;

public class Graph {
  private int adjMat[][];
  private int numberOfNodes;

  public static final Integer INFINITY = Integer.MAX_VALUE;

  public Graph(int[][] adjMat, int numberOfNodes) {
    this.adjMat = adjMat;
    this.numberOfNodes = numberOfNodes;
  }

  public ArrayList<Edge> edgeList() {
    ArrayList<Edge> edgeList = new ArrayList<>();
    for (int i = 0; i < numberOfNodes; i++) {
      for (int j = 0; j < numberOfNodes; j++) {
        if (adjMat[i][j] != -1) edgeList.add(new Edge(i,j,adjMat[i][j]));
      }
    }
    return edgeList;
  }

  public Graph bellmanFord(int startNode) {
    int[] d = new int[numberOfNodes];
    int[] p = new int[numberOfNodes];
    for (int i = 0 ; i < numberOfNodes; i++) {
      d[i] = (i == startNode) ? 0 : INFINITY;
      p[i] = (i == startNode) ? 0 : -1;
    }
    ArrayList<Edge> edgeList = edgeList();
    for (int i = 0; i < numberOfNodes; i++) {
      for (Edge edge : edgeList) {
        if (d[edge.origin] + edge.weight < d[edge.destination]) {
          d[edge.destination] = d[edge.origin] + edge.weight;
          p[edge.destination] = edge.origin;
        }
      }
    }
    return constructMinimumSpanningTree(p,d);
  }

  public Graph dijkstra(int startNode) {
    int[] d = new int[numberOfNodes];
    int[] p = new int[numberOfNodes];
    DistanceQueue<Integer, Integer> q = new DistanceQueue<>();
    for (int i = 0; i < numberOfNodes; i++) {
      d[i] = (i == startNode) ? 0 : INFINITY;
      p[i] = (i == startNode) ? 0 : -1;
      q.put(i, d[i]);
    }
    // Working up to this point

    while(!q.isEmpty()) {
      int u = q.removeMinValue();
      for (int v : getAdjacentNodes(u)) {
        if (!q.containsKey(v)) continue;
        if (d[u] + weight(u,v) < d[v]) {
          d[v] = d[u] + weight(u,v);
          p[v] = u;
        }
        q.replace(v, d[v]);
      }
    }

    return constructMinimumSpanningTree(p,d);
  }
  public Graph dagSearch(int startNode) {
    int[] d = new int[numberOfNodes];
    int[] p = new int[numberOfNodes];
    for (int i = 0; i < numberOfNodes; i++) {
      d[i] = (i == startNode) ? 0 : INFINITY;
      p[i] = (i == startNode) ? 0 : -1;
    }

    for (int u = 0; u < numberOfNodes; u++) {
      for (Edge e : getOutEdges(u)) {
        int r = d[u] + e.weight;
        if (r < d[e.destination]) {
          d[e.destination] = r;
          p[e.destination] = u;
        }
      }
    }
    return constructMinimumSpanningTree(p, d);
  }

  public List<Edge> getOutEdges(int u) {
    ArrayList<Edge> toReturn = new ArrayList<>();
    for (int i = 0; i < adjMat[u].length; i++) {
      if (adjMat[u][i] != -1) {
        toReturn.add(new Edge(u, i, adjMat[u][i]));
      }
    }
    return toReturn;
  }

  public Graph constructMinimumSpanningTree(int[] p, int[] d) {
    int[][] adjMat = new int[numberOfNodes][numberOfNodes];
    for (int i = 0; i < p.length; i++) {
      adjMat[p[i]][i] = weight(p[i], i);
    }
    for (int i = 0; i < d.length; i++) {
      adjMat[i][i] = d[i];
    }

    return new Graph(adjMat, numberOfNodes);
  }

  public int weight(int node1, int node2) {
    return adjMat[node1][node2];
  }

  public List<Integer> getAdjacentNodes(int node) {
    ArrayList<Integer> adj = new ArrayList<>();
    for (int i = 0; i < adjMat[node].length; i++) {
      if (adjMat[node][i] != -1) adj.add(i);
    }
    return adj;
  }

  @Override
  public String toString() {
    String toReturn = "";
    for (int i = 0; i < numberOfNodes; i++) {
      for (int j = 0; j < numberOfNodes-1; j++) {
        toReturn += adjMat[i][j] + " ";
      }
      toReturn += adjMat[i][numberOfNodes-1] + "\n";
    }
    return toReturn;
  }
}
