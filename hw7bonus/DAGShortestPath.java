import java.util.*;
import java.io.*;

public class DAGShortestPath {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.print("Enter input file name: ");
    Scanner fileScan = null;
    try {
      fileScan = new Scanner(new File(scan.nextLine()));
    }
    catch(FileNotFoundException e) {
      System.out.println("File Not Found");
    }

    ArrayList<ArrayList<Integer>> adjMatArrayList = new ArrayList<>();
    while (fileScan.hasNextLine()) {
      String[] line = fileScan.nextLine().split(" ");
      ArrayList<Integer> l = new ArrayList<>();
      for (int i = 0; i < line.length; i++) {
        int toAdd = (line[i].equals("inf")) ? -1 : Integer.parseInt(line[i]);
        l.add(toAdd);
      }
      adjMatArrayList.add(l);
    }

    int numberOfNodes = adjMatArrayList.size();
    int[][] adjMat = new int[numberOfNodes][numberOfNodes];

    for (int i = 0; i < numberOfNodes; i++) {
      for (int j = 0; j < numberOfNodes; j++) {
        adjMat[i][j] = adjMatArrayList.get(i).get(j);
      }
    }

    Graph g = new Graph(adjMat, numberOfNodes);
    Graph dagResult = g.dagSearch(0);
    System.out.println(dagResult);
  }
}
