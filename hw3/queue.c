#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"

struct Node* createNewNode(int data) {
  struct Node* newNode = (struct Node*) malloc(sizeof(struct Node));
  newNode->data = data;
  newNode->next = NULL;
}

void insert(Queue *queue, int data) {
  if (queue->size >= queue->capacity) printf("Queue Full\n");
  else {
    Node *newNode = createNewNode(data);
    if (queue->head == NULL) {
      queue->head = newNode;
      queue->tail = newNode;
      newNode->next = newNode;
    }
    else {
      queue->tail->next = newNode;
      queue->tail = newNode;
      newNode->next = queue->head;
    }

    queue->size++;
  }
}

void delete(Queue *queue) {
  Node *tmp = queue->head;
  queue->head = tmp->next;
  queue->tail->next = queue->head;
  free(tmp);
  queue->size--;
}

void printQueue(Queue *queue) {
  if (queue->head == NULL) printf("(empty queue)\n");
  else {
    Node *cur = queue->head;
    while (cur->next != queue->head) {
      printf("%d, ", cur->data);
      cur = cur->next;
    }
    printf("%d\n", cur->data);
  }
}

int main() {
  char inputFileName[100];

  printf("Enter your input file name: ");
  scanf("%s", inputFileName);

  FILE *inputFile;
  inputFile = fopen(inputFileName, "r");

  if (inputFile == NULL) {
    printf("File not found");
    exit(1);
  }

  char command[10];

  Queue *queue = (Queue*)malloc(sizeof(Queue));
  queue->head = NULL;
  queue->tail = NULL;
  queue->size = 0;

  fscanf(inputFile, "%s", command);
  queue->capacity = atoi(command);

  while(fscanf(inputFile, "%s", command) != EOF) {
    printf("Queue before operation: ");
    printQueue(queue);

    printf("Performing operation (%s)\n", command);

    char *first;
    first = strtok(command, ".");

    if (strcmp(first, "del") == 0) {
      delete(queue);
    }
    else {
      int data = atoi(first);
      insert(queue, data);
    }
    printf("Queue after operation: ");
    printQueue(queue);

    printf("\n");
  }

  fclose(inputFile);

  return 0;
}
