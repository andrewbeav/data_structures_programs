#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linked_list.h"


Node* createNewNode(int data) {
  Node* newNode = (Node*)malloc(sizeof(Node));

  newNode->data = data;
  newNode->prev = NULL;
  newNode->next = NULL;
}

void printList(LinkedList* linkedList) {
  Node* cur = linkedList->header->next;

  if (cur->next == NULL) printf("(empty list)");

  while (cur->next != NULL) {
    printf("%d", cur->data);

    cur = cur->next;
    
    if (cur->next != NULL) {
      printf(", ");
    }
  }
  printf("\n");
}

int search(LinkedList *linkedList, int data) {
  Node *cur = linkedList->header->next;

  while (cur->data != data && cur->next != NULL) {
    cur = cur->next;
  }

  if (cur->next == NULL) return 0;

  return 1;
}

void insert(LinkedList *linkedList, int data, int targetData) {
  if (search(linkedList, data) == 1) printf("Error: Duplicate Key\n");
  else {
    Node *cur = linkedList->header->next;

    while (cur->data != targetData && cur->next != NULL) {
      cur = cur->next;
    }

    if (cur->next == NULL) printf("Not Found\n");
    else {
      Node *newNode = createNewNode(data);
      Node *tmp = cur->next;
      cur->next = newNode;
      newNode->prev = cur;
      newNode->next = tmp;
      tmp->prev = newNode;
    }
  }
}


void insertAtHead(LinkedList *linkedList, int data) {
  if (search(linkedList, data) == 1) printf("Error: Duplicate Key\n");
  else {
    Node *newNode = createNewNode(data);
    newNode->next = linkedList->header->next;
    linkedList->header->next->prev = newNode;
    newNode->prev = linkedList->header;
    linkedList->header->next = newNode;
  }
}

/* delete node that contains that data */
void delete(LinkedList* linkedList, int data) {
  Node* cur = linkedList->header->next;

  while(cur->data != data) {
    cur = cur->next;
  } 

  cur->prev->next = cur->next;
  cur->next->prev = cur->prev;

  free(cur);

  linkedList->size--;
}

LinkedList* instantiateList() {
  Node *header = createNewNode(0);
  Node *trailer = createNewNode(0);
  header->next = trailer;
  trailer->prev = header;
  LinkedList *linkedList = (LinkedList*)malloc(sizeof(LinkedList));
  linkedList->header = header;
  linkedList->trailer = trailer;
  linkedList->size = 0;
  return linkedList;
}

int main() {
  LinkedList* linkedList = instantiateList();

  char inputFileName[100];

  printf("Enter your input file name: ");
  scanf("%s", inputFileName);

  FILE *inputFile;
  inputFile = fopen(inputFileName, "r");

  if (inputFile == NULL) {
    printf("File not found");
    exit(1);
  }

  char command[10];

  while(fscanf(inputFile, "%s", command) != EOF) {
    printf("List before operation: ");
    printList(linkedList);
    printf("Running operation (%s)\n", command);

    char *num_str;
    char *op;
    num_str = strtok(command, ".");
    int data = atoi(num_str);
    op = strtok(NULL, ".");

    char *operation;
    char *pos_str;
    operation = strtok(op, "_");
    pos_str = strtok(NULL, "_");
    int pos = 0;
    if (pos_str != NULL) {
      pos = atoi(pos_str);
    }

    if (strcmp(operation, "in") == 0) {
      if (pos_str != NULL) {
        insert(linkedList, data, pos);
      }
      else {
        insertAtHead(linkedList, data);
      }
    }
    else if (strcmp(operation, "del") == 0) {
      delete(linkedList, data);
    }
    else if (strcmp(operation, "sch") == 0) {
      if (search(linkedList, data) == 1) printf("Output: found\n");
      else printf("Output: not found\n");
      printf("\n");
      continue;
    }

    printf("List after operation: ");
    printList(linkedList);

    printf("\n");
  }

  fclose(inputFile);

  return 0;
}
