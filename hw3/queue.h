typedef struct Node {
  int data;
  struct Node *next;
} Node;

typedef struct Queue {
  struct Node *head;
  struct Node *tail;
  int size;
  int capacity;
} Queue;
