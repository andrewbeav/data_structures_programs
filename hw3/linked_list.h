typedef struct Node {
  int data;
  struct Node *prev;
  struct Node *next;
} Node;

typedef struct LinkedList {
  int size;
  Node *header;
  Node *trailer;
} LinkedList;
