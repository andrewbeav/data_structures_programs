typedef struct Node {
  int data;
  struct Node *below;
} Node;

typedef struct Stack {
  Node *top;
  int capacity;
  int size;
} Stack;
