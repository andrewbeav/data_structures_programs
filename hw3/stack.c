#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

Node* createNewNode(int data) {
  Node* newNode = (Node*)malloc(sizeof(Node));

  newNode->data = data;
  newNode->below = NULL;
}

void printStack(Stack *stack) {
  if (stack->top == NULL) printf("(empty stack)");
  else {
    Node* cur = stack->top;

    while (cur != NULL) {
      printf("%d", cur->data);
      if (cur->below != NULL) printf(", ");

      cur = cur->below;
    }
  }

  printf("\n");
}

void push(Stack *stack, int data) {
  if (stack->size >= stack->capacity) {
    printf("Stack Full\n");
  }
  else {
    Node* newNode = createNewNode(data);
    newNode->below = stack->top;
    stack->top = newNode;
    stack->size++;
  }
}

int pop(Stack *stack) {
  int toReturn = stack->top->data;
  Node* tmp = stack->top;
  stack->top = stack->top->below;
  free(tmp);

  stack->size--;
  return toReturn;
}

int main() {
  char inputFileName[100];

  printf("Enter your input file name: ");
  scanf("%s", inputFileName);

  FILE *inputFile;
  inputFile = fopen(inputFileName, "r");

  if (inputFile == NULL) {
    printf("File not found");
    exit(1);
  }

  char command[10];

  Stack *stack = (Stack*)malloc(sizeof(Stack));

  fscanf(inputFile, "%s", command);
  stack->capacity = atoi(command);

  while(fscanf(inputFile, "%s", command) != EOF) {
    printf("Stack before operation: ");
    printStack(stack);

    printf("Performing operation (%s)\n", command);

    char *first;
    first = strtok(command, ".");

    if (strcmp(first, "pop") == 0) {
      printf("Output: %d\n", pop(stack));
    }
    else {
      int data = atoi(first);
      push(stack, data);
    }
    printf("Stack after operation: ");
    printStack(stack);

    printf("\n");
  }

  fclose(inputFile);

  return 0;
}
