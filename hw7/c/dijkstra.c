#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph_algorithms.h"
#include "priority_queue.h"

int main() {
  char input_file_name[100];
  printf("Enter your input file name: ");
  scanf("%s", input_file_name);

  FILE *input_file;
  input_file = fopen(input_file_name, "r");

  if (input_file == NULL) {
    printf("Input File Not Found\n");
    exit(1);
  }

  int m, n;
  fscanf(input_file, "%d %d", &m, &n);


  int adj[m][n];
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      char entry[10];
      fscanf(input_file, "%s", entry);
      adj[i][j] = (strcmp(entry, "inf") == 0) ? -1 : atoi(entry);
    }
  }

  Graph *graph = init_new_graph(**adj, m);
  print_graph_matrix(graph);
  
  return 0;
}
