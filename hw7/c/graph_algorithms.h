#include <stdlib.h>

typedef struct Graph {
  int number_of_nodes;
  int **adj_mat;
} Graph;

Graph *init_new_graph(int *adj_mat, int number_of_nodes) {
  Graph *graph = (Graph*)malloc(sizeof(Graph));
  graph->number_of_nodes = number_of_nodes;
  graph->adj_mat = adj_mat;
  return graph;
}

void print_graph_matrix(Graph *g) {
  for (int i = 0; i < g->number_of_nodes; i++) {
    for (int j = 0; j < g->number_of_nodes; j++) {
      printf("%d, ", g->adj_mat[i]);
    }
    printf("\n");
  }
}
