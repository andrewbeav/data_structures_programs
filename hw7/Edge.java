public class Edge {
  public int origin, destination, weight;
  public Edge(int origin, int destination, int weight) {
    this.origin = origin;
    this.destination = destination;
    this.weight = weight;
  }

  public String toString() {
    return "(" + this.origin + "," + this.destination + "," + this.weight + ")";
  }
}
