import java.util.*;

class DistanceQueue<K,V extends Comparable<? super V>> extends HashMap<K,V> {
  public DistanceQueue() {
    super();
  }

  public K removeMinValue() {
    V min = null;
    K toReturn = null;
    for (K key : this.keySet()) {
      if (min == null || this.get(key).compareTo(min) < 0) {
        min = this.get(key);
        toReturn = key;
      }
    }
    this.remove(toReturn);
    return toReturn;
  }
}
