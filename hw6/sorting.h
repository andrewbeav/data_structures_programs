#include <time.h>

#define MERGE 0
#define BUCKET 1

/* Helper Methods */
void print_array(int *a, int length) {
  for (int i = 0; i < length-1; i++) {
    printf("%d, ", a[i]);
  }
  printf("%d\n", a[length-1]);
}

int find_max(int *a, int length) {
  int max = a[0];
  for (int i = 1; i < length; i++) {
    if (a[i] > max) max = a[i];
  }
  return max;
}

int get_length_of_range(int a_length, int start, int end) {
  if (end < 0) end = a_length + end;
  return (end+1-start < a_length) ? end+1-start : a_length-start;
}

int *copy_of_range(int *a, int a_length, int start, int end) {
  if (end < 0) end = a_length + end;
  int new_length = get_length_of_range(a_length, start, end);
  int *to_return = (int*)calloc(new_length, sizeof(int));
  for (int i = start; i <= end && i < a_length; i++) {
    to_return[i-start] = a[i];
  }
  return to_return;
}

/* Merge Sort */
void merge(int *a, int *l, int l_size, int *r, int r_size) {
  int i1 = 0, i2 = 0, i = 0;

  while(i1 < l_size && i2 < r_size) {
    if(l[i1] < r[i2]) a[i++] = l[i1++];
    else a[i++] = r[i2++];
  }
  while (i1 < l_size) a[i++] = l[i1++];
  while (i2 < r_size) a[i++] = r[i2++];
}

void merge_sort(int *a, int length) {
  if (length > 1) {
    int mid = length/2;
    int *l = (int*)calloc(mid, sizeof(int));
    int *r = (int*)calloc(length-mid, sizeof(int));

    for (int i = 0; i < mid; i++) l[i] = a[i];
    for (int i = mid; i < length; i++) r[i-mid] = a[i];

    merge_sort(l, mid);
    merge_sort(r, length-mid);
    merge(a, l, mid, r, length-mid);

    free(l);
    free(r);
  }
}

/* Bucket Sort */
void bucket_sort(int *a, int length) {
  int N = find_max(a, length);
  int *b = (int*)calloc(N+1, sizeof(int));

  for (int i = 0; i < length; i++) {
    b[a[i]] += 1;
  }

  int a_index = 0;
  for (int i = 0; i < N+1; i++) {
    for (int j = 0; j < b[i]; j++) a[a_index++] = i;
  }

  free(b);
}

/* Single "Sort" Method */
void sort(int *a, int length, int method) {
  switch(method) {
    case MERGE:
      merge_sort(a, length);
      break;
    case BUCKET:
      bucket_sort(a, length);
  }
}


/* Methods used for collecting data to generate plot */
int *partition(int *a, int a_length, int partition_size) {
  int *to_return = (int*)calloc(partition_size, sizeof(int));
  for (int i = 0; i < partition_size && i < a_length; i++) {
    to_return[i] = a[i];
  }
  return to_return;
}

long timediff(struct timespec t1, struct timespec t2) {
  long t1_nsec = (long)(t1.tv_sec*1000000000) + t1.tv_nsec;
  long t2_nsec = (long)(t2.tv_sec*1000000000) + t2.tv_nsec;
  return t2_nsec - t1_nsec;
}

void gen_data_file(int *a, int length, int sorting_method, char *file_name) {
  FILE *output_file;
  output_file = fopen(file_name, "w");

  if (output_file == NULL) {
    printf("Error creating output file\n");
    return;
  }

  fprintf(output_file, "n,time\n");

  struct timespec t1, t2;
  for (int i = 1000; i < length; i += 1000) {
    int *to_sort = partition(a, length, i);
    printf("Sorting %d elements\n", i);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t1);
    sort(to_sort, i, sorting_method);
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &t2);
    fprintf(output_file, "%d,%ld\n", i, timediff(t1, t2));
  }
}
