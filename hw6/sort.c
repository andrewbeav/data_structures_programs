#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sorting.h"

int main(int argc, char *argv[]) {

  if (argc < 2) {
    printf("Usage:\n");
    printf("./merge_sort.out SORTING_METHOD [-d]\n");
    printf("\nAvailable sorting methods: bucket, merge\n");
    printf("\nIf -d flag present, a csv file will be generated containing runtime data for the chosen algorithm, see README for details\n");
    printf("\nInput file specifications:\n");
    printf("Input file must contain a list of integers, where the first integer is the number of integers that is going to be in the array, and the rest define the array to be sorted.\n");
    exit(1);
  }

  char input_file_name[100];

  printf("Enter your input file name: ");
  scanf("%s", input_file_name);

  FILE *input_file;
  input_file = fopen(input_file_name, "r");

  if (input_file == NULL) {
    printf("File not found");
    exit(1);
  }

  int n;
  fscanf(input_file, "%d", &n);

  int a[n];
  for (int i = 0; i < n; i++) {
    if (fscanf(input_file, "%d", &a[i]) == EOF) {
      printf("Wrong n provided\n");
      exit(1);
    }
  }
  int method = (strcmp(argv[1], "merge") == 0) ? MERGE : BUCKET;

  if (argc >= 3 && strcmp(argv[2], "-d") == 0) {
    printf("Enter name of output file to be generated: ");
    char out_file_name[100];
    scanf("%s", out_file_name);
    gen_data_file(a, n, method, out_file_name);
  }
  else {
    printf("Enter range (start, end seperated by comma): ");
    int start, end;
    scanf("%d,%d", &start, &end);
    sort(a, n, method);
    int range_length = get_length_of_range(n, start, end);
    print_array(copy_of_range(a, n, start, end), range_length);
  }

  return 0;
}
