import random
import sys

file = open('input.txt', 'w')
n = int(sys.argv[1])

file.write(str(n) + " ")
for _ in range(n):
    file.write(str(random.randint(0,32767)) + " ")
